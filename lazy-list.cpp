// Implementation of a lazy array, can represent infinite functions

#include <functional>
#include <array>

template <typename T>
class
LazyList
{
private:
    // Function used to generate the infinite list
    const std::function<T(size_t idx)> _func;

public:
    template <typename Fn>
    LazyList(Fn func) :
        _func{func} {}

    // Allow for indexing the list
    T
    operator[](size_t idx) const
    {
        return _func(idx);
    }

    // Gets a subset of the infinite data
    // Returns an empty list if the start is before the end
    template <size_t len>
    std::array<T, len>
    sublist(size_t startidx, size_t endidx) const
    {
        if (startidx > endidx) return std::array<T, len>();

        std::array<T, len> out;

        for(size_t i{startidx}; i <= endidx; i++)
            out[i-startidx] = _func(i);

        return out;
    }
};

#ifdef __DEBUGMODE__
#include <iostream>

int
main ()
{
    LazyList<size_t> l([](size_t idx){return idx * idx;});
    auto sublist = l.sublist<17>(0,16);

    std::cout << '[' << std::endl;

    for(auto i : sublist)
        std::cout << ", " << i << std::endl;

    std::cout << ']' << std::endl;
}

#endif
