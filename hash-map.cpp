#include <iostream>
#include <sstream>
#include <algorithm>

template <typename K, typename V>
class
HashMap
{
private:
    size_t _size;

    K** _keys;
    V** _vals;

    int // Find key by index. Returns -1 if it could not find the given key
    find_key_idx(K key) const
    {
        for(int i{}; i < _size; i++)
        {
            if (_keys[i] == nullptr)
                continue;

            if (*_keys[i] == key)
                return i;
        }

        return -1;
    }

    int // Same as above, but for values
    find_val_idx(V val) const
    {
        for(int i{}; i < _size; i++)
        {
            if (_vals[i] == nullptr)
                continue;

            if (*_vals[i] == val)
                return i;
        }

        return -1;
    }

public:
    HashMap(size_t size)
    {
        _size = size;
        _keys = new K*[size];
        _vals = new V*[size];

        std::fill(_keys, _keys+size, nullptr);
        std::fill(_vals, _vals+size, nullptr);
    }

    ~HashMap()
    {
        delete [] _keys;
        delete [] _vals;
    }

    V*
    operator[](K key) const
    {
        if(find_key_idx(key) != -1)
            return _vals[find_key_idx(key)];

        for(int i{}; i < _size; i++)
        {
            if(_keys[i] == nullptr)
            {
                _keys[i] = new K(key);
                _vals[i] = new V;

                return _vals[i];
            }
        }

        return nullptr;
    }

    HashMap&
    push(K key, V val)
    {
        *operator[](key) = val;
        return *this;
    }

    HashMap&
    resize(size_t size, bool allowShrinking = false)
    {
        if (size > _size && !allowShrinking) {
            throw("Attempted to shrink a hashmap. This can lead to loss of data. This can be overriden with the allowshrinking parameter");
        }

        K** new_keys = new K*[size];
        V** new_vals = new V*[size];

        std::copy(_keys, _keys+_size, new_keys);
        std::copy(_vals, _vals+_size, new_vals);

        delete [] _keys;
        delete [] _vals;

        _size = size;
        _keys = new_keys;
        _vals = new_vals;

        return *this;
    }

    HashMap&
    clear()
    {
        std::fill(_keys, _keys+_size, nullptr);
        std::fill(_vals, _vals+_size, nullptr);

        return *this;
    }

    HashMap&
    remove(K key)
    {
        int idx = find_key_idx(key);
        if (idx < 0) return *this;

        delete _keys[idx];
        delete _vals[idx];

        for (int i{idx}; i < _size-1; i++)
        {
            _keys[i] = _keys[i+1];
            _vals[i] = _vals[i+1];
        }

        _keys[_size-1] = nullptr;
        _vals[_size-1] = nullptr;

        return *this;
    }

    std::string
    to_string() const
    {
        std::stringstream out;
        out << "HashMap[" << _size << "]<";

        for (int i{}; i < _size; i++)
        {
            if(_keys[i] == nullptr) continue;

            if(i > 0) out << ", ";

            out << *_keys[i] << ":" << *_vals[i];
        }

        out << ">";

        return out.str();
    }

    friend std::ostream&
    operator<<(std::ostream &out, const HashMap &map) {
        out << map.to_string();
        return out;
    }
};

#ifdef __DEBUGMODE__
#include <iostream>

int
main ()
{

    // Assignments
    HashMap<std::string, int> assign_hash_map(2);

    *assign_hash_map["One"] = 1;
    assign_hash_map.push("Two", 2);

    std::cout << "TEST::Assignments: " << assign_hash_map << std::endl;

    // Resizing
    HashMap<std::string, int> resize_hash_map(1);

    resize_hash_map
        .push("One", 1)
        .resize(2)
        .push("Two", 2);

    std::cout << "TEST::Resize: " << resize_hash_map << std::endl;

    // Clearing
    HashMap<std::string, int> clear_hash_map(1);

    clear_hash_map
        .push("One", 1)
        .clear();
    std::cout << "TEST::Clear: " << clear_hash_map << std::endl;

    // Removing
    HashMap<std::string, int> remove_hash_map(5);

    remove_hash_map
        .push("One", 1)
        .push("Two", 2)
        .push("Three", 3)
        .push("Four", 4)
        .push("Five", 5)
        .remove("Two");

    std::cout << "TEST::Remove: " << remove_hash_map << std::endl;

    return 0;
}
#endif
